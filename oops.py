# Python oops concepts

# Employee details program to demonstrate Class, Method and Object concept
# 1. Class
class Employee:
    def __init__(self, f_name, l_name, age):
        self.first_name = f_name
        self.last_name = l_name
        self.age = age

    # 2.Method
    def details(self):
        print(self.first_name, self.last_name, self.age)

# 3.Object, here employee is an object of class Employee
employee = Employee("Sushmita", "M", 24)
employee.details()


# Person details program to demonstrate Inheritance concept.
#  4.Inheritance
class Person:
  def __init__(self, f_name, l_name, age):
    self.first_name = f_name
    self.last_name = l_name
    self.age = age

  def printname(self):
    print(self.first_name, self.last_name, self.age)

class Employee(Person):
  pass

object = Employee("Sushmita", "M", 24)
object.printname()


# User details program to demonstrate Polymorphism concept.
# 5.Polymorphism
class User1:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def user_info(self):
        print("My name is " + (self.name) + ". I am " + str(self.age) + " years old.")

class User2:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def user_info(self):
        print("My name is " + (self.name) + ". I am " + str(self.age) + " years old.")

user1 = User1("Sushmita", 24)
user2 = User2("Silla", 25)

for user in (user1, user2):
    user.user_info()


# Vehicle selling price program to demonstrate Data Abstraction and Encapsulation concept.
#  6.Data Abstraction and 7.Encapsulation
class Vehicle:

    def __init__(self):
        self.__max_price = 50000

    def sell(self):
        print("Selling Price: {}".format(self.__max_price))

    def set_max_price(self, price):
        self.__max_price = price

vehicle = Vehicle()
vehicle.sell()

# change the max_price
vehicle.__max_price = 60000 #as we are trying to change the max_price but python treats
                            # max_price as private attribute we can't change the value.
vehicle.sell()

# using setter function
vehicle.set_max_price(45000)
vehicle.sell()
